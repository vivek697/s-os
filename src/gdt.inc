
;	Gdt.inc
 
 
bits	16
 
;*******************************************
; InstallGDT()
;	- Install our GDT
;*******************************************
 
InstallGDT:
 
	cli				; clear interrupts
	pusha				; save registers
	mov si,dmsg1
	call displaymessage
	lgdt 	[toc]			; load GDT into GDTR
	mov si,dmsg2
	call displaymessage
	sti				; enable interrupts
	popa				; restore registers
	ret				; All done!
 
;*******************************************
; Global Descriptor Table (GDT)
;*******************************************
dmsg1 db 0x0D, 0x0A, "Copying GDT...", 0x0D, 0x0A, 0x00          
dmsg2 db 0x0D, 0x0A, "GDT Copied!", 0x0D, 0x0A, 0x00   

gdt_data: 
	dd 0 				; null descriptor
	dd 0 
 
; gdt code:				; code descriptor
	dw 0FFFFh 			; limit low
	dw 0 				; base low
	db 0 				; base middle
	db 10011010b 			; access
	db 11001111b 			; granularity
	db 0 				; base high
 
; gdt data:				; data descriptor
	dw 0FFFFh 			; limit low (Same as code)
	dw 0 				; base low
	db 0 				; base middle
	db 10010010b 			; access
	db 11001111b 			; granularity
	db 0				; base high
 
end_of_gdt:
toc: 
	dw end_of_gdt - gdt_data - 1 	; limit (Size of GDT)
	dd gdt_data 			; base of GDT
 
 
%define NULL_DESC 0
%define CODE_DESC 0x8
%define DATA_DESC 0x10

 
