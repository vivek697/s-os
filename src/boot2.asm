[BITS 16]
[ORG 0x500]

jmp stage2
 %include "/media/8C1C94AD1C949434/os project/working/src/functions2.inc"  

stage2:
          cli                                       ;disable interrupts
          xor     ax, ax
          mov     ds, ax
          mov     es, ax
          mov     fs, ax
          mov     gs, ax

; create stack
          mov     ax, 0x9000
          mov     ss, ax
          mov     sp, 0xFFFF
          sti



; post message
printmes:
          mov     si,msgHello
          call    displaymessage
          mov     si, msgEnd
          call    displaymessage
          ;hlt  
	
loop1:
	mov si,prompt
	call displaymessage		;display prompt

	mov si,input_str
	call getstring   		;got usercmd in input_str

	mov si,mycmd
	call makenull			;initialize mycmd
	
	mov si,input_str
	mov di,mycmd

	mov al,'R'			;check if command='R'
	mov  [di],al
	call stringcmp
	cmp ah,1
		jz true_r
	mov al, 'r'			;check if command='r'
	mov  [di],al
	call stringcmp
	cmp ah,1
		jnz false_r
		true_r :  call restart   ;if al=='r' || al=='R' call restart
		false_r:  ;check for c
			  mov al,'C'
			  mov [di],al
			  call stringcmp
			  cmp ah,1
				jz true_c
			  mov al,'c'
			  mov [di],al
			  call stringcmp
			  cmp ah,1
				jnz false_c
			
				true_c :  jmp stage3	;jump to new sector
				false_c:  mov si,msgbadcmd
					call displaymessage

jmp loop1



;
;**************************************************************************************************
; data section
msgHello  db 0x0D, 0x0A, "Hello World", 0x0D, 0x0A, 0x00          
msgEnd  db 0x0D, 0x0A, "OS Project!", 0x0D, 0x0A, 0x00           
msgkey db 0x0d, 0x0a, "  ",0x0d,0x0a,0x00						
msgbadcmd db 0x0d, 0x0a, "Bad Command Try again",0x0D,0x0A,0x00
prompt db 0x0d, 0x0a, "sos :>", 0x0
input_str resb 100 
mycmd resb 100 
;**************************************************************************************************

;**************************************************************************************************
;						Stage-3 
;**************************************************************************************************
	%include "/media/8C1C94AD1C949434/os project/working/src/gdt.inc"  
	%include "/media/8C1C94AD1C949434/os project/working/src/A20.inc"  
bits 16
goto_pmode:
	mov si,msgEnd3
	call displaymessage
	mov si,contmsg
	call displaymessage
	call getkey
	cli		;disable interrupts to prevent triple faults
	mov eax, cr0  
	or al, 1	;set bit0 in cr0 
	mov cr0, eax
	
	mov ax,DATA_DESC
	mov ds,ax
	mov es,ax
	mov fs,ax
	mov gs,ax
	mov ss,ax
	jmp CODE_DESC:stage4

stage3:
          mov     si,msgHello3
          call    displaymessage

	; Moving to Protected Mode		
		call InstallGDT					; Install GDT

	;Enable A20 if not already enabled
		call check_a20
		cmp ax,1
		jz a20enabled
		call EnableA20_Protect				;Enable A20
		call check_a20
		cmp ax,1
		jnz error_a20					;error in enabling a20

	a20enabled:
	        mov     si, msga20success
	        call    displaymessage
		jmp goto_pmode		
	error_a20:
		mov si,a20errormsg
		call displaymessage
		cli
		hlt

;**************************************************************************************************
; data section
msgHello3  db 0x0D, 0x0A, "Bootloader Stage 3 Loading....", 0x0D, 0x0A, 0x00          
msga20success  db 0x0D, 0x0A, "A20 was successfully enabled!", 0x0D, 0x0A, 0x00
msgEnd3  db 0x0D, 0x0A, "Switching to Protected Mode...", 0x0D, 0x0A, 0x00
a20errormsg  db 0x0D, 0x0A, "Error! Unable to enable A20....", 0x0D, 0x0A, 0x00             
contmsg  db 0x0D, 0x0A, "Press any key to continue...", 0x0D, 0x0A, 0x00  
;**************************************************************************************************

;**************************************************************************************************
;						Stage-4 
;**************************************************************************************************

bits 32
	%include "/media/8C1C94AD1C949434/os project/working/src/stdio.inc"
stage4:
 
	mov		esp, 90000h		; stack begins from 90000h
 
	;---------------------------------------;
	;   Clear screen and print success	;
	;---------------------------------------;
 
	call		ClrScr32
	mov		ebx, msgHello4
	call		Puts32
	mov		ebx, msgEnd4
	call		Puts32
 
 
;*******************************************************
;	Stop execution
;*******************************************************
 
STOP:
	cli
	hlt

;**************************************************************************************************
; data section
msgHello4  db 0x0D, 0x0A, "Bootloader Stage 4 Loading....", 0x0D, 0x0A, 0x00          
msgEnd4  db 0x0D, 0x0A, "Welcome to 32 Bit mode", 0x0D, 0x0A, 0x00   
TIMES 1536-($-$$) DB 0
