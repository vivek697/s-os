;**************************************************************************************************
; Display Message function to display character with starting offset in si
displaymessage1:
          lodsb                                       ; load next character
          or      al, al                              ; test for NUL character
          jz      .DONE
	  call    printkey1
          jmp     displaymessage1
     .DONE:
          ret          
;**************************************************************************************************

;printkey function to display key in tty mode...al=ascii code and bh=page no
printkey1:
          mov     ah, 0x0E                            ; BIOS teletype
          mov     bh, 0x00                            ; display page 0

          mov     bl, 0x07                            ; text attribute
          int     0x10                                ; invoke BIOS
	ret
;***************************************************************************************************
