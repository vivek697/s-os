Bits 16
;**************************************************************************************************
; Display Message function to display character with starting offset in si
displaymessage:
          lodsb                                       ; load next character
          or      al, al                              ; test for NUL character
          jz      .DONE
	  call    printkey
          jmp     displaymessage
     .DONE:
          ret          
;**************************************************************************************************
Bits 16
;printkey function to display key in tty mode...al=ascii code and bh=page no
printkey:
          mov     ah, 0x0E                            ; BIOS teletype
          mov     bh, 0x00                            ; display page 0

          mov     bl, 0x07                            ; text attribute
          int     0x10                                ; invoke BIOS
	ret
;***************************************************************************************************
;***************************************************************************************************
Bits 16
;getkey function to get charactor from keyboard
getkey:
	xor ah,ah
	int 16h
      ret
;***************************************************************************************************
Bits 16
;restart function to restart
restart:
	jmp 0ffffh:0000h
      ret
;****************************************************************************************************
Bits 16
;take string as an input.. (a set of chars until the user pressed Enter key)
getstring:
	call getkey
	cmp al,0x8
	jz backspace
	mov [si], al
	call printkey
	cmp al, 0xd
	jz return
	inc si
	jmp getstring
return: 
	mov al,0x0a
	call printkey
	xor al,al
	mov [si],al
	ret
backspace:
	dec si
	mov al,[si]
	cmp al,0x0
	jz nopermit
	mov al,0x8
	call printkey	;move one step back (on screen)	
;	dec si		;go back one step (on string)
	mov al,0x0	;let the new key press be null charactor
	mov [si], al	;clear the last charactor(on string)
	call printkey	;clear the last charactor(on screen)
	mov al,0x8	;press backspace
	call printkey	;move back one step(on screen)
	jmp getstring
nopermit:
	inc si
	jmp getstring	
;*******************************************************************************************************
Bits 16
;stringcmp function to compare 2 strings with offsets in di and si
stringcmp:
	mov al,[di]
	mov bl,[si]
	cmp al,0
	jz nulldi
	cmp al,bl
	jnz mismatch
	inc di
	inc si
	jmp stringcmp
	mismatch: 
		mov ah,0
		ret
	nulldi:
		cmp bl,0
		jz match
		jmp mismatch
	match:
		mov ah,1
		ret
;*************************************************************************************************************
Bits 16
;makenull convert string with offset at si, of 100 char to null
makenull:
	xor al,al
	mov cx,100
	nullify:	mov [si],al
	inc si
	loop nullify
      ret
