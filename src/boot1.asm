[BITS 16]
[ORG 0x7C00]

jmp start
 %include "/media/8C1C94AD1C949434/os project/working/src/oem.inc"  
 %include "/media/8C1C94AD1C949434/os project/working/src/functions1.inc"  

start:
; code located at 0000:7C00, adjust segment registers
          cli                                       ;disable interrupts
          mov     ax, 0x0000
          mov     ds, ax
          mov     es, ax
          mov     fs, ax
          mov     gs, ax

; create stack
          mov     ax, 0x0000
          mov     ss, ax
          mov     sp, 0xFFFF
          sti

;loader
reset:	
	mov ah,0	;reset floppy disk function
	mov dl,0	;drive 0 is floppy drive
	int 0x13	;call BIOS
	jc reset	;carry flag is set in case of error

         mov     si,msgreset
         call    displaymessage1

	mov ax,0x50	;location to load the new sector
	mov es,ax
	xor bx,bx
	
	mov ah,0x02	;read floppy sector function
	mov al,0x03	;read 3 sectors (boot2.asm)
	mov ch,0x00	;track no is 1
	mov cl,0x02	;sector no is 2
	mov dh,0	;head number
	mov dl,0	;floppy drive no is 0
	int 0x13	;call bios to read sector
	jnc  read
	jmp reset
	read:
          mov     si,msgread
          call    displaymessage1	
	  jmp 0x50:0x00	;jump to new sector
	 ;DW 0xEA,0x00
          ;DW 0x00,0xE0
          ;DB 0x07
;*/
;**************************************************************************************************
; data section
msgreset  db 0x0D, 0x0A, "Floppy Reset Success", 0x0D, 0x0A, 0x00          
msgread  db 0x0D, 0x0A, "Floppy sector 2 Read success!", 0x0D, 0x0A, 0x00           
;ASM Signature
          TIMES 510-($-$$) DB 0
          DW 0xAA55
